# WEATHER APPLICATION
This application provides weather information and forecasts for a specific city/town or region.

The API used to fetch the weather information is [Open-Meteo](https://open-meteo.com/en/docs)

## Features
- [ ] Current forecast - Displays the temperature for the current hour.
- [ ] Daily forecast - Displays the temperature for the given day. It shows the lowest and highest temperature for the day.
- [ ] Weekly forecast - Displays the weather information for the next 7 days
- [ ] Average weekly temperature - Displays the average temperature for the next 7 days
- [ ] Weather conditions - Different weather conditions (eg. rain, snow, wind, frost, fog, sunshine) will be displayed in addition to the temperature for each forecast.

### Running the application
The aim is to run the application in a terminal, you write e.g. 
- ***python weatherApp.py Stockholm *** and you get the current weather information about Stockholm.
- ***python weatherApp.py stockholm daily*** and you get the daily forecast for Stockholm.
- ***python weatherApp.py Stockholm week*** and you get the weather forecast for the next 7 days for Stockholm.
- ***python weatherApp.py Stockholm average*** and you get the average temperature for the next 7 days for Stockholm.

You should also be able to choose the date e.g. ***python weatherApp.py Stockholm 2023-05-31*** and it tells you the weather information for the 31 May 2023