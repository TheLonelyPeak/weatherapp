"""Weather application that provides weather
information and forecasts for a specific city"""
from geopy.geocoders import Nominatim
import requests


def get_coordinates(city):
    """Returns a coordinate (latitude, longitude)for the given city.
    The coordinates returned are rounded to two decimals"""
    geolocator = Nominatim(user_agent="weather_app")  # geolocator object
    location = geolocator.geocode(city)  # get the geo-information of the city
    coordinates = {"latt": round(location.latitude, 2),
                   "long": round(location.longitude, 2)}
    return coordinates


def get_current_temperature(city):
    """Returns the weather forecast information of the given city"""
    coordinates = get_coordinates(city)
    latt = coordinates["latt"]
    long = coordinates["long"]

    # forecast information including temperature, precipitation probability and weathercode
    results = requests.get(f"https://api.open-meteo.com/v1/forecast?latitude={latt}&longitude={long}&current_weather=true&precipitation_probability,weathercode&forecast_days=1&timezone=auto", timeout=3)
    if results.status_code == 200:
        return results.json()

    return results.status_code


def get_daily_forecast(city):
    """Returns the daily weather forecast information of the given city.
    It contains the lowest and highest temperature for the day"""
    coordinates = get_coordinates(city)
    latt = coordinates["latt"]
    long = coordinates["long"]

    # forecast information including max and min temperature for the day, uv index, precipitation probability and weathercode
    results = requests.get(f'https://api.open-meteo.com/v1/forecast?latitude={latt}&longitude={long}&daily=weathercode,temperature_2m_max,temperature_2m_min,uv_index_max,precipitation_probability_max&forecast_days=1&timezone=auto', timeout=3)
    if results.status_code == 200:
        return results.json()

    return results.status_code


def get_weekly_forecast(city):
    """Returns weather forecast information for the next 7 days"""
    coordinates = get_coordinates(city)
    latt = coordinates["latt"]
    long = coordinates["long"]

    # forecast information including max and min temperature, uv index, precipitation probability and weathercode for the next 7 days
    results = requests.get(f'https://api.open-meteo.com/v1/forecast?latitude={latt}&longitude={long}&daily=weathercode,temperature_2m_max,temperature_2m_min,uv_index_max,precipitation_probability_max&forecast_days=8&timezone=auto', timeout=3)
    if results.status_code == 200:
        return results.json()

    return results.status_code
