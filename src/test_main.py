""" Unnittest for the weather application
"""
import unittest
import main as weather_app


class TestStringMethods(unittest.TestCase):
    """Unittest test class"""

    def test_get_coordinates(self):
        """Test the Get_Coordinates function. it expects a coordinate for a given city"""
        expected = {"latt": 59.33, "long": 18.07}
        self.assertEqual(weather_app.get_coordinates("Stockholm"), expected)

    def test_get_current_temp(self):
        """This test checks if a temperature is returned"""
        get_temp = weather_app.get_current_temperature("stockholm")
        temp = get_temp['current_weather']["temperature"]

        self.assertTrue(temp)

    def test_get_daily_forecast(self):
        """This test checks if a forecast for the whole day is returned"""
        get_forecast = weather_app.get_daily_forecast("stockholm")
        forecast = get_forecast['daily']

        self.assertTrue(forecast)

    def test_get_weekly_forecast(self):
        """This test checks if a forecast for the whole week is returned"""
        get_forecast = weather_app.get_weekly_forecast("stockholm")
        forecast = get_forecast['daily']

        self.assertTrue(forecast)


if __name__ == '__main__':
    unittest.main()
